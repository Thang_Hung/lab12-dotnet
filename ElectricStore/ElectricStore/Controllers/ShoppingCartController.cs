﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ElectricStore.Data;
using ElectricStore.Models;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace ElectricStore.Controllers
{
    public class ShoppingCartController : Controller
    {
        private StoreContext _context;
        private List<Product> products;
        private SessionStateViewModel sessionModel;

        public ShoppingCartController(StoreContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            if (string.IsNullOrEmpty(HttpContext.Session.GetString("CustomerFirstName")) == false
                && string.IsNullOrEmpty(HttpContext.Session.GetString("CustomerProducts")) == false)
            {
                List<int> productsListId = JsonConvert.DeserializeObject<List<int>>(HttpContext.Session.GetString("CustomerProducts"));                products = new List<Product>();

                foreach(var item in productsListId)
                {
                    var product = _context.Products.SingleOrDefault(p => p.Id == item);
                    products.Add(product);
                }

                sessionModel = new SessionStateViewModel();
                sessionModel.CustomerName = HttpContext.Session.GetString("CustomerFirstName");
                sessionModel.SelectedProducts = products;
            }

            return View(sessionModel);
        }

        public IActionResult Chat()
        {
            return View();
        }
    }
}